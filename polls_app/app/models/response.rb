class Response < ActiveRecord::Base
  attr_accessible :respondent_id, :answer_choice_id

  validates :respondent_id, :presence => true
  validates :answer_choice_id, :presence => true
  validate :respondent_has_not_already_answered_question
  validate :no_author_rigging

  belongs_to :respondent,
             :foreign_key => :respondent_id,
             :primary_key => :id,
             :class_name => "User"

   belongs_to :answer_choice,
              :foreign_key => :answer_choice_id,
              :primary_key => :id,
              :class_name => "AnswerChoice"


  private
    def respondent_has_not_already_answered_question
      response = existing_responses
      unless response.count == 0 || ( response.count == 1 && response.first.id == self.id )
        errors[:respondent_id] = 'respondents can only answer questions once'
      end
   end

   def existing_responses
     Response.find_by_sql([<<-SQL, self.respondent_id, self.answer_choice.question_id])
     SELECT
      responses.*
     FROM
      responses
     INNER JOIN
      answer_choices
     ON
      responses.answer_choice_id = answer_choices.id
     WHERE
      responses.respondent_id = ? AND answer_choices.question_id = ?;
     SQL
   end

   def no_author_rigging
     current_poll_author = self.answer_choice.question.poll

     if respondent == User.
       joins(:authored_polls).
       joins("JOIN questions ON questions.poll_id = polls.id").
       joins("JOIN answer_choices ON questions.id = answer_choices.question_id").uniq.
       where("users.id = ?", current_poll_author )[0]

       errors[:respondent_id] = "Can't answer your own poll."
     end
   end

end
