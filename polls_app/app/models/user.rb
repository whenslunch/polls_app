class User < ActiveRecord::Base
  attr_accessible :name
  validates :name, :uniqueness => true, :presence => true


  has_many :authored_polls,
           :foreign_key => :author_id,
           :primary_key => :id,
           :class_name => "Poll"

   has_many :responses,
            :foreign_key =>
            :respondent_id,
            :primary_key => :id,
            :class_name => "Response"

   has_many :answer_choices, :through => :responses, :source => :answer_choice


   def answered_questions #completed all the questions in the polls.
     Question.includes(:poll).find_by_sql([<<-SQL, self.id])
     SELECT
       *
     FROM
       questions
     INNER JOIN
       answer_choices ON questions.id = answer_choices.question_id
     INNER JOIN
       responses ON answer_choices.id = responses.answer_choice_id
     WHERE
       responses.respondent_id = ?
     SQL
   end

   def completed_polls
     completed_polls = []

     poll_map.each do |poll_id, answered_questions_count|
       poll = Poll.find(poll_id)
       if poll.questions.count == answered_questions_count
         completed_polls << poll
       end
     end

     completed_polls
   end

   def poll_map
     poll_map = Hash.new(0)
     answered_questions.each do |question|
       poll_map[question.poll_id] += 1
     end
     poll_map
   end


   def uncompleted_polls
     poll_map.keys - completed_polls
   end

end


