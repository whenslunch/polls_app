class Question < ActiveRecord::Base
   attr_accessible :text
   validates :text, :presence => true
   validates :poll_id, :presence => true

   belongs_to :poll,
              :foreign_key => :poll_id,
              :primary_key => :id,
              :class_name => "Poll"

   has_many :answer_choices,
            :foreign_key => :question_id,
            :primary_key => :id,
            :class_name => "AnswerChoice",
            :dependent => :destroy


  def results
    puts self.text
    answer_choices = self.answer_choices.includes(:responses)

    question_responses = Hash.new(0)

    answer_choices.each { |answer| question_responses[answer.text] = answer.responses.length }

    question_responses
  end
end

