# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


u1 = User.create!({ :name => "BenW" })
u2 = User.create!({ :name => "BenL" })
u3 = User.create!({ :name => "BenH" })

p u1.authored_polls
p1 = u1.authored_polls.create!({ :title => "President" })

p u2.authored_polls
p2 = u2.authored_polls.create!({ :title => "Senate" })

p1.questions.create!({ :text => "Which Prez?" })
p1.questions.last.answer_choices.create!({ :text => "Dem" }  )        #id 1
p1.questions.last.answer_choices.create!({ :text => "Repub" })        #id 2

p1.questions.create!({ :text => "Pro Obama?"})
p1.questions.last.answer_choices.create!({ :text => "Yes" }  )        #id 3
p1.questions.last.answer_choices.create!({ :text => "No" })           #id 4

p1.questions.create!({ :text => "Pro George W?"})
p1.questions.last.answer_choices.create!({ :text => "Yes" }   )       #id 5
p1.questions.last.answer_choices.create!({ :text => "no" })           #id 6

p2.questions.create!({ :text => "Which Party?"})
p2.questions.last.answer_choices.create!({ :text => "Dems" })
p2.questions.last.answer_choices.create!({ :text => "Repub" })


p2.questions.create!({ :text => "Third Party?"})
p2.questions.last.answer_choices.create!({ :text => "Yes" })
p2.questions.last.answer_choices.create!({ :text => "no" })


p2.questions.create!({ :text => "Do you <3 Boener?" })
p2.questions.last.answer_choices.create!({ :text => "Yes" })
p2.questions.last.answer_choices.create!({ :text => "no" })

Response.create!({ :respondent_id => u2.id, :answer_choice_id => 1 })
# Response.create!{ :respondent_id => u1.id, :answer_choice_id => 3 }
Response.create!({ :respondent_id => u2.id, :answer_choice_id => 5 })

Response.create!({ :respondent_id => u1.id, :answer_choice_id => 7 })
# Response.create!{ :respondent_id => u2.id, :answer_choice_id => 9 }
Response.create!({ :respondent_id => u1.id, :answer_choice_id => 11 })